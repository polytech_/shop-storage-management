package Controller.Ajout;


import Controller.Gestion.GestionProduitVendeurController;
import DAO.ProductDAO;
import Model.Product;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.Node;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;

import javafx.stage.Stage;

import java.sql.SQLException;

import static Alert.Alert.allertDialog;

public class AjoutProduitVendeurController {

    private ProductDAO productDAO = new ProductDAO();

    private GestionProduitVendeurController gestionProduitVendeurController;

    public void setGestionProduitVendeurController(GestionProduitVendeurController gestionProduitController) {
        this.gestionProduitVendeurController = gestionProduitController;
    }

    @FXML
    public Label utilisateurLabel;

    @FXML
    private TextField nameProductTextField;

    @FXML
    private TextField storageTextField;

    @FXML
    private TextField priceTextField;

    @FXML
    private Label labelSection;



    public void gestionProduitVendeur(ActionEvent actionEvent) throws SQLException {

        if(!nameProductTextField.getText().isEmpty() && (!priceTextField.getText().isEmpty()) &&
                (!storageTextField.getText().isEmpty())){

            //Ajout du produit
            Product product = new Product(nameProductTextField.getText(),Double.parseDouble(priceTextField.getText()),
                    Long.parseLong(storageTextField.getText()),labelSection.getText());

            productDAO.addProduct(product);

            gestionProduitVendeurController.getDonnees().add(product);

            Node node = (Node) actionEvent.getSource();
            Stage stage =(Stage)node.getScene().getWindow();
            //Fermeture de celle-ci
            stage.close();

        }else{
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci d'insérer toutes les données");
        }

    }

    public Labeled getLabelSection() {
        return labelSection;
    }

    public Labeled getLabelUserLogged() {
        return utilisateurLabel;
    }


}

