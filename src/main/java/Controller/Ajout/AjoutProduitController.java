package Controller.Ajout;

import Controller.Gestion.GestionProduitController;
import DAO.ProductDAO;
import DAO.SectionDAO;
import Model.Product;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.sql.SQLException;

import static Alert.Alert.allertDialog;


public class AjoutProduitController {

    private ProductDAO productDAO = new ProductDAO();
    private SectionDAO sectionDAO = new SectionDAO();

    private GestionProduitController gestionProduitController;

    public void setGestionProduitController(GestionProduitController gestionProduitController) {
        this.gestionProduitController = gestionProduitController;
    }


    @FXML
    public Label utilisateurLabel;

    @FXML
    private TextField nameProductTextField;

    @FXML
    private TextField storageTextField;

    @FXML
    private TextField priceTextField;

    @FXML
    private ComboBox<String> sectionCBX;

    public void init() throws SQLException {

        sectionCBX.setItems(FXCollections.observableArrayList(sectionDAO.listSections()));

    }

    public void ajoutProduit(ActionEvent actionEvent) throws SQLException {

        if(!nameProductTextField.getText().isEmpty() && (!priceTextField.getText().isEmpty()) &&
                (!storageTextField.getText().isEmpty()) && (!sectionCBX.getSelectionModel().getSelectedItem().isEmpty())){

            Product product = new Product(nameProductTextField.getText(), Double.parseDouble(priceTextField.getText()),
                    Long.parseLong(storageTextField.getText()), sectionCBX.getSelectionModel().getSelectedItem());

            //Ajout du produit
            productDAO.addProduct(product);

            //Ajout du produit dans la table principale de gestion des produits
            gestionProduitController.getDonnees().add(product);


            Node node = (Node) actionEvent.getSource();
            Stage stage =(Stage)node.getScene().getWindow();
            //Fermeture de celle-ci
            stage.close();


            //On integre le nouveau produit dans le fichier dataSet.json

        }else{
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci d'insérer toutes les données");
        }

    }

    public Labeled getLabelUserLogged() {
        return utilisateurLabel;
    }


}
