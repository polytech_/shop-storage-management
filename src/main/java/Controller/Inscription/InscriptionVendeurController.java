package Controller.Inscription;

import Controller.Gestion.GestionVendeurController;
import DAO.EmployeeDAO;
import DAO.SectionDAO;
import Model.SalesMan;


import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static Alert.Alert.allertDialog;

public class InscriptionVendeurController {

    private SectionDAO sectionDAO = new SectionDAO();

    private EmployeeDAO employeeDAO = new EmployeeDAO();

    GestionVendeurController gestionVendeurController;

    public void setGestionVendeurController(GestionVendeurController gestionVendeurController) {
        this.gestionVendeurController = gestionVendeurController;
    }

    @FXML
    private TextField lastnameTextField;

    @FXML
    private PasswordField passwordPassField;

    @FXML
    private TextField usernameTextField;

    @FXML
    private ComboBox<String> sectionComboBox;

    public void init() throws SQLException {
        sectionComboBox.setItems(FXCollections.observableArrayList(sectionDAO.listSections()));
    }

    @FXML
    public void ajoutVendeur(ActionEvent event) throws SQLException {
        //Longueur des chaines username & password
        int lengthpass = passwordPassField.getText().length();
        int lengthid = usernameTextField.getText().length();

        //Recherche de carac spéciaux
        Pattern p = Pattern.compile("[^ \\w]");
        Matcher m = p.matcher(usernameTextField.getText());

        //Tests av inscrption
        if((!lastnameTextField.getText().isEmpty()) && (!passwordPassField.getText().isEmpty())
                && (!usernameTextField.getText().isEmpty())) {

            if(sectionComboBox.getSelectionModel().getSelectedItem()==null) {
                allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner un rayon");
                return;
            }

            if(lengthid >= 4 && lengthpass >= 6 && (!m.find())){
            SalesMan salesManToAdd = new SalesMan(
                            lastnameTextField.getText(),
                            usernameTextField.getText(),passwordPassField.getText(),
                            sectionComboBox.getSelectionModel().getSelectedItem());

            employeeDAO.addSalesMen(salesManToAdd);

            gestionVendeurController.getDonnees().add(salesManToAdd);

            allertDialog("Succès", Alert.AlertType.INFORMATION, "Vendeur ajouté !");

            //Récupération de la fenetre
            Node node = (Node) event.getSource();
            Stage stage =(Stage)node.getScene().getWindow();
            //Fermeture de celle-ci
            stage.close();
            }else{
                allertDialog("Erreur", Alert.AlertType.ERROR,"Nom d'utilisateur ou mot de passe non conforme !");
            }

        }else{
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci d'insérer toutes les données");
        }
    }


}
