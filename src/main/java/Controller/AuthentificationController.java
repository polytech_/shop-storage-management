package Controller;

import Controller.Gestion.GestionProduitController;
import Controller.Gestion.GestionProduitVendeurController;
import DAO.EmployeeDAO;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

import static Alert.Alert.allertDialog;

public class AuthentificationController {

    private static final String TYPE_ERR = "Erreur";

    private static final String MSG_ERR = "Nom d'utilisateur ou mot de passe incorrect !";

    private EmployeeDAO employeeDAO = new EmployeeDAO();

    @FXML
    private ComboBox<String> typeuserComboBox;

    @FXML
    private PasswordField passwordPassField;

    @FXML
    private TextField usernameTextField;


    public void initialize(){
        typeuserComboBox.getItems().addAll("Manager","Vendeur");
    }

    @FXML
    void logInGestion(ActionEvent event) throws IOException, SQLException {

        String title = "Gestion de produits";

        if((!passwordPassField.getText().isEmpty()) && (!usernameTextField.getText().isEmpty())){

            if((typeuserComboBox.getSelectionModel().getSelectedItem()==null)){
                allertDialog(TYPE_ERR, Alert.AlertType.ERROR, "Merci de sélectionner un type d'employé");
            }

            else if((typeuserComboBox.getSelectionModel().getSelectedItem().equals("Manager"))){

                String usernameInput = usernameTextField.getText();
                String pswdInputText = passwordPassField.getText();

                if(employeeDAO.loginVerifManager(usernameInput, pswdInputText)) {

                    Stage st = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gestion/GestionProduit.fxml"));
                    Region root = loader.load();

                    //Récupération de la fenetre de login
                    Node node = (Node) event.getSource();
                    Stage stage =(Stage)node.getScene().getWindow();
                    //Fermeture de celle-ci
                    stage.close();

                    //Initialisation
                    GestionProduitController gestionProduitController = loader.<GestionProduitController>getController();
                    gestionProduitController.getLabelUserLogged().setText(usernameInput);
                    gestionProduitController.init();

                    Scene scene = new Scene(root);
                    st.setScene(scene);
                    st.initModality(Modality.APPLICATION_MODAL);
                    st.setResizable(false);
                    st.setTitle(title);
                    st.show();

                } else {
                    allertDialog(TYPE_ERR, Alert.AlertType.ERROR, MSG_ERR);
                }

            } else if((typeuserComboBox.getSelectionModel().getSelectedItem().equals("Vendeur"))) {

                String usernameInput = usernameTextField.getText();
                String pswdInputText = passwordPassField.getText();

                if(employeeDAO.loginVerifSalesMan(usernameInput, pswdInputText)){

                    Stage st = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gestion/GestionProduitVendeur.fxml"));
                    Region root = loader.load();

                    //Récupération de la fenetre de login
                    Node node = (Node) event.getSource();
                    Stage stage =(Stage)node.getScene().getWindow();
                    //Fermeture de celle-ci
                    stage.close();

                    //Initialisation
                    GestionProduitVendeurController gestionProduitVendeurController = loader.<GestionProduitVendeurController>getController();
                    gestionProduitVendeurController.getLabelUserLogged().setText(usernameInput);
                    gestionProduitVendeurController.init();

                    Scene scene = new Scene(root);
                    st.setScene(scene);
                    st.initModality(Modality.APPLICATION_MODAL);
                    st.setResizable(false);
                    st.setTitle(title);
                    st.show();

                } else {
                    allertDialog(TYPE_ERR, Alert.AlertType.ERROR, MSG_ERR);
                }
            }


        } else {
            allertDialog(TYPE_ERR, Alert.AlertType.ERROR, MSG_ERR);
        }
    }


    @FXML
    void inscription(ActionEvent event) throws IOException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Inscription/InscriptionType.fxml"));
        Region root = loader.load();
        Node node = (Node) event.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Gestion de produits");
        st.show();
    }

}
