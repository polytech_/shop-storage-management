package Controller.Gestion;

import DAO.EmployeeDAO;
import DAO.SectionDAO;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

import static Alert.Alert.allertDialog;

public class TransfertVendeurController {


    private EmployeeDAO employeeDAO = new EmployeeDAO();

    private SectionDAO sectionDAO = new SectionDAO();

    private GestionVendeurController gestionVendeurController;

    public void setGestionVendeurController(GestionVendeurController gestionVendeurController) {
        this.gestionVendeurController = gestionVendeurController;
    }

    @FXML
    private TextField nameSalesManTextField;

    @FXML
    private Label utilisateurLabel;

    @FXML
    private TextField oldShelfTxt;

    @FXML
    private ComboBox<String> newShelfCBX;

    public void init() throws SQLException {
        newShelfCBX.setItems(FXCollections.observableArrayList(sectionDAO.listSections()));
    }


    @FXML
    void transfertSalesMan(ActionEvent event) throws SQLException {
        //Modification du produit

        String username = employeeDAO.getUsernameOfSalesMan(nameSalesManTextField.getText());
        String newShelf = newShelfCBX.getSelectionModel().getSelectedItem();

        if(newShelf!= null) {
            employeeDAO.transferSalesMan(username, newShelf);

            //Mise à jour de la table de gestion des produits
            gestionVendeurController.getDonnees().clear();
            gestionVendeurController.getDonnees().setAll(employeeDAO.listSalesMen());


            //On récupère la fenêtre en cours
            Node node = (Node) event.getSource();
            Stage stage = (Stage) node.getScene().getWindow();
            //On la ferme
            stage.close();

            allertDialog("Succès", Alert.AlertType.INFORMATION, "Vendeur transferé !");
        } else allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner le nouveau rayon");



    }


    public Label getUtilisateurLabel() {
        return utilisateurLabel;
    }

    public TextField getOldShelfTxt() {
        return oldShelfTxt;
    }

    public TextField getNameSalesManTextField() {
        return nameSalesManTextField;
    }
}
