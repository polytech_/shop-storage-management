package Controller.Gestion;

import Controller.Ajout.AjoutProduitController;
import Controller.Modification.ModifierProduitController;
import Controller.Modification.TransfererProduitController;

import DAO.ProductDAO;
import Model.Product;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

import static Alert.Alert.allertDialog;

public class GestionProduitController {

    private static Logger logger = Logger.getLogger("myLogger");


    private ProductDAO productDAO = new ProductDAO();

    private ObservableList<Product> donnees;

    public ObservableList<Product> getDonnees() {
        return donnees;
    }

    public void setDonnees(ObservableList<Product> donnees) {
        this.donnees = donnees;
    }

    @FXML
    private Label labelUserLogged;

    @FXML
    private TableColumn<Product, String> gpColumnProduit;

    @FXML
    private TableColumn<Product, String> gpColumnInSection;

    @FXML
    private TableColumn<Product, Double> gpColumnPrix;

    @FXML
    private TableColumn<Product, Long> gpColumnStock;

    @FXML
    private TableView<Product> productTableView;


    public void init() throws SQLException {

        setDonnees(FXCollections.observableArrayList(productDAO.listProducts()));

        gpColumnProduit.setCellValueFactory(new PropertyValueFactory<>("nameProduct"));
        gpColumnInSection.setCellValueFactory(new PropertyValueFactory<>("inSection"));
        gpColumnPrix.setCellValueFactory(new PropertyValueFactory<>("price"));
        gpColumnStock.setCellValueFactory(new PropertyValueFactory<>("storage"));

        productTableView.setItems(donnees);

        productTableView.setRowFactory( tv -> {
            TableRow<Product> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                //On récupère la ligne selectionnée
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {

                        Stage st = new Stage();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Modification/TransfertProduit.fxml"));
                        Region root = null;
                        try {
                            root = loader.load();

                            //Initialisation
                            TransfererProduitController transfererProduitController = loader.<TransfererProduitController>getController();
                            transfererProduitController.getOldShelfTxt().setText(productTableView.getSelectionModel().getSelectedItem().getInSection());
                            transfererProduitController.getNameProductTextField().setText(productTableView.getSelectionModel().getSelectedItem().getNameProduct());
                            transfererProduitController.getPriceTextField().setText(Double.toString(productTableView.getSelectionModel().getSelectedItem().getPrice()));
                            transfererProduitController.getStorageTextField().setText(Long.toString(productTableView.getSelectionModel().getSelectedItem().getStorage()));

                            transfererProduitController.getLabelUserLogged().setText(this.getLabelUserLogged().getText());
                            transfererProduitController.setGestionProduitController(this);
                            transfererProduitController.init();

                            Scene scene = new Scene(root);
                            st.setScene(scene);
                            st.initModality(Modality.APPLICATION_MODAL);
                            st.setResizable(false);
                            st.setTitle("Modification d'un produit");
                            st.show();

                        } catch (IOException | SQLException e) { logger.info(e.getMessage()); }

                    }
            });
            return row ;
        });
    }

    @FXML
    void gestionVendeurWindow() throws IOException, SQLException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Gestion/GestionVendeur.fxml"));
        Region root = loader.load();


        //Initialisation
        GestionVendeurController gestionVendeurController = loader.<GestionVendeurController>getController();
        gestionVendeurController.getLabelUserLogged().setText(labelUserLogged.getText());
        gestionVendeurController.init();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Gestion Vendeur");
        st.show();
    }

    public void ajoutProduit() throws IOException, SQLException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Ajout/AjoutProduit.fxml"));
        Region root = loader.load();

        AjoutProduitController ajoutProduitController = loader.<AjoutProduitController>getController();
        ajoutProduitController.setGestionProduitController(this);
        ajoutProduitController.getLabelUserLogged().setText(labelUserLogged.getText());
        ajoutProduitController.init();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Ajout d'un produit");
        st.show();

    }

    @FXML
    void suppressionProduit(ActionEvent event) throws SQLException {

        Product selectedProduct = productTableView.getSelectionModel().getSelectedItem();

        if (selectedProduct != null) {
            productDAO.deleteProduct(selectedProduct.getNameProduct());
            donnees.remove(selectedProduct);
        }

        else {
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner un produit");
        }

    }

    public void logOut(ActionEvent actionEvent) throws IOException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Authentification.fxml"));
        Region root = loader.load();
        Node node = (Node) actionEvent.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Authentification");
        st.show();
    }

    @FXML
    void modifierProduit() {

        if(productTableView.getSelectionModel().getSelectedItem()!=null) {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Modification/ModifierProduit.fxml"));
            Region root = null;
            try {
                root = loader.load();

                //Initialisation
                ModifierProduitController modifierProduitController = loader.<ModifierProduitController>getController();
                modifierProduitController.getSectionTxt().setText(productTableView.getSelectionModel().getSelectedItem().getInSection());
                modifierProduitController.getNameProductTextField().setText(productTableView.getSelectionModel().getSelectedItem().getNameProduct());
                modifierProduitController.getPriceTextField().setText(Double.toString(productTableView.getSelectionModel().getSelectedItem().getPrice()));
                modifierProduitController.getStorageTextField().setText(Long.toString(productTableView.getSelectionModel().getSelectedItem().getStorage()));

                modifierProduitController.getUtilisateurLabel().setText(this.getLabelUserLogged().getText());
                modifierProduitController.setGestionProduitController(this);

                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setResizable(false);
                stage.setTitle("Modification d'un produit");
                stage.show();

            } catch (IOException e) { logger.info(e.getMessage()); }

        }

        else {
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner un produit !");
        }




    }

    public Labeled getLabelUserLogged() {
        return labelUserLogged;
    }


}
