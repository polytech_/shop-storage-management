package Controller.Gestion;

import Controller.Inscription.InscriptionVendeurController;
import DAO.EmployeeDAO;
import Model.SalesMan;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

import static Alert.Alert.allertDialog;

public class GestionVendeurController {

    private static Logger logger = Logger.getLogger("myLogger");

    private EmployeeDAO employeeDAO = new EmployeeDAO();

    private ObservableList<SalesMan> donnees;

    public ObservableList<SalesMan> getDonnees() {
        return donnees;
    }

    public void setDonnees(ObservableList<SalesMan> donnees) {
        this.donnees = donnees;
    }


    @FXML
    private Label labelUserLogged;

    @FXML
    private TableView<SalesMan> sellerTableView = new TableView<>();

    @FXML
    private TableColumn<SalesMan, String> gsColumnName;

    @FXML
    private TableColumn<SalesMan, String> gsColumnRayon;

    @FXML
    private Button btnAjoutVendeur;

    @FXML
    private Button btnLogOut;


    public void init() throws SQLException {

        setDonnees(FXCollections.observableArrayList(employeeDAO.listSalesMen()));

        gsColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        gsColumnRayon.setCellValueFactory(new PropertyValueFactory<>("nameSection"));

        sellerTableView.setItems(donnees);
        
        sellerTableView.setRowFactory( tv -> {
            TableRow<SalesMan> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                //On récupère la ligne selectionnée
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {

                    Stage st = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Modification/TransfertVendeur.fxml"));
                    Region root = null;
                    try {
                        root = loader.load();

                        //Initialisation
                        TransfertVendeurController transfertVendeurController = loader.<TransfertVendeurController>getController();
                        transfertVendeurController.getOldShelfTxt().setText(sellerTableView.getSelectionModel().getSelectedItem().getNameSection());
                        transfertVendeurController.getUtilisateurLabel().setText(labelUserLogged.getText());
                        transfertVendeurController.getNameSalesManTextField().setText(sellerTableView.getSelectionModel().getSelectedItem().getName());
                        transfertVendeurController.getUtilisateurLabel().setText(this.getLabelUserLogged().getText());
                        transfertVendeurController.setGestionVendeurController(this);
                        transfertVendeurController.init();

                        Scene scene = new Scene(root);
                        st.setScene(scene);
                        st.initModality(Modality.APPLICATION_MODAL);
                        st.setResizable(false);
                        st.setTitle("Transfert d'un vendeur");
                        st.show();

                    } catch (IOException | SQLException e) { logger.info(e.getMessage());}

                }

            });
            return row ;
        });
    
    }


    @FXML
    void ajoutVendeur() throws IOException, SQLException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Inscription/InscriptionVendeur.fxml"));
        Region root = loader.load();

        //Initialisation
        InscriptionVendeurController inscriptionVendeurController = loader.<InscriptionVendeurController>getController();
        inscriptionVendeurController.setGestionVendeurController(this);
        inscriptionVendeurController.init();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Ajout d'un vendeur");
        st.show();
    }


    @FXML
    void suppressionVendeur() throws SQLException {

        SalesMan selectedSalesMan = sellerTableView.getSelectionModel().getSelectedItem();

        if (selectedSalesMan != null) {
            employeeDAO.deleteSalesMan(selectedSalesMan.getUsername());
            donnees.remove(selectedSalesMan);
        }

        else {
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner un vendeur");

        }
    }


    public Labeled getLabelUserLogged() {
        return labelUserLogged;
    }
}
