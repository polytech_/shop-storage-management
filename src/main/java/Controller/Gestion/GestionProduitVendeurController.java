package Controller.Gestion;
import Controller.Ajout.AjoutProduitVendeurController;
import Controller.Modification.ModifierProduitController;
import DAO.EmployeeDAO;
import DAO.ProductDAO;
import Model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;


import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

import static Alert.Alert.allertDialog;

public class GestionProduitVendeurController {

    private static Logger logger = Logger.getLogger("myLogger");

    private ProductDAO productDAO = new ProductDAO();

    private EmployeeDAO employeeDAO = new EmployeeDAO();

    private ObservableList<Product> donnees;

    public ObservableList<Product> getDonnees() {
        return donnees;
    }

    public void setDonnees(ObservableList<Product> donnees) {
        this.donnees = donnees;
    }

    @FXML
    private Label labelUserLogged;

    @FXML
    private TableColumn<Product, String> gpvColumnProduit;

    @FXML
    private TableColumn<Product, String> gpvColumnInSection;

    @FXML
    private TableColumn<Product, Double> gpvColumnPrix;

    @FXML
    private TableColumn<Product, Long> gpvColumnStock;

    @FXML
    private TableView<Product> productvTableView;



    public void init() throws SQLException {

        setDonnees(FXCollections.observableArrayList(productDAO.listProducts()));

        gpvColumnProduit.setCellValueFactory(new PropertyValueFactory<>("nameProduct"));
        gpvColumnPrix.setCellValueFactory(new PropertyValueFactory<>("price"));
        gpvColumnStock.setCellValueFactory(new PropertyValueFactory<>("storage"));
        gpvColumnInSection.setCellValueFactory(new PropertyValueFactory<>("inSection"));

        productvTableView.setItems(donnees);

        productvTableView.setRowFactory(tv -> {
            TableRow<Product> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                //On récupère la ligne selectionnée
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {

                    Stage stage = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/Modification/ModifierProduitVendeur.fxml"));
                    Region root = null;
                    try {
                        root = loader.load();

                        //Initialisation
                        ModifierProduitController modifierProduitController = loader.<ModifierProduitController>getController();
                        modifierProduitController.getSectionTxt().setText(productvTableView.getSelectionModel().getSelectedItem().getInSection());
                        modifierProduitController.getNameProductTextField().setText(productvTableView.getSelectionModel().getSelectedItem().getNameProduct());
                        modifierProduitController.getPriceTextField().setText(Double.toString(productvTableView.getSelectionModel().getSelectedItem().getPrice()));
                        modifierProduitController.getStorageTextField().setText(Long.toString(productvTableView.getSelectionModel().getSelectedItem().getStorage()));

                        modifierProduitController.getUtilisateurLabel().setText(this.getLabelUserLogged().getText());
                        modifierProduitController.setGestionProduitVendeurController(this);

                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setResizable(false);
                        stage.setTitle("Modification d'un produit");
                        stage.show();

                    } catch (IOException e) { logger.info(e.getMessage()); }

                }
            });
            return row ;
        });

    }

    public void logOut(ActionEvent actionEvent) throws IOException {
        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Authentification.fxml"));
        Region root = loader.load();
        Node node = (Node) actionEvent.getSource();
        Stage stage =(Stage)node.getScene().getWindow();
        //Fermeture de celle-ci
        stage.close();

        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Authentification");
        st.show();
    }

    public void ajoutProduit() throws IOException, SQLException {

        Stage st = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Ajout/AjoutProduitVendeur.fxml"));
        Region root = loader.load();

        //Initialisation
        AjoutProduitVendeurController ajoutProduitVendeurController = loader.<AjoutProduitVendeurController>getController();
        ajoutProduitVendeurController.getLabelSection().setText(employeeDAO.getSectionOfSalesMan(labelUserLogged.getText()));
        ajoutProduitVendeurController.setGestionProduitVendeurController(this);
        ajoutProduitVendeurController.getLabelUserLogged().setText(this.getLabelUserLogged().getText());


        Scene scene = new Scene(root);
        st.setScene(scene);
        st.initModality(Modality.APPLICATION_MODAL);
        st.setResizable(false);
        st.setTitle("Ajout d'un produit");
        st.show();

    }

    public Label getLabelUserLogged() {
        return labelUserLogged;
    }

    public TableView<Product> getProductvTableView(){
        return productvTableView;
    }



    public void suppressionProduit() throws SQLException {

        Product selectedProduct = productvTableView.getSelectionModel().getSelectedItem();

        if (selectedProduct != null) {
            productDAO.deleteProduct(selectedProduct.getNameProduct());
            donnees.remove(selectedProduct);
        }
        else {
            allertDialog("Erreur", Alert.AlertType.ERROR, "Merci de sélectionner un produit");
        }
    }


}
