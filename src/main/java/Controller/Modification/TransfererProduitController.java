package Controller.Modification;

import Controller.Gestion.GestionProduitController;

import DAO.ProductDAO;
import DAO.SectionDAO;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.sql.SQLException;
import static Alert.Alert.allertDialog;


public class TransfererProduitController {


    private SectionDAO sectionDAO = new SectionDAO();

    private ProductDAO productDAO = new ProductDAO();

    GestionProduitController gestionProduitController;

    public void setGestionProduitController(GestionProduitController gestionProduitController) {
        this.gestionProduitController = gestionProduitController;
    }


    @FXML
    public TextField nameProductTextField;

    @FXML
    public TextField storageTextField;

    @FXML
    public TextField priceTextField;

    @FXML
    public ComboBox<String> newShelfCBX;

    @FXML
    public Label utilisateurLabel;

    @FXML
    private TextField oldShelfTxt;


    public void init() throws SQLException {
        newShelfCBX.setItems(FXCollections.observableArrayList(sectionDAO.listSections()));
    }


    public void transfererProduit(ActionEvent actionEvent) throws SQLException {

        //Modification du produit
        productDAO.transferProduct(nameProductTextField.getText(), newShelfCBX.getSelectionModel().getSelectedItem());

        //Mise à jour de la table de gestion des produits
        gestionProduitController.getDonnees().clear();
        gestionProduitController.getDonnees().setAll(productDAO.listProducts());

        //On récupère la fenêtre en cours
        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        allertDialog("Succès", Alert.AlertType.INFORMATION, "Produit transferé !");
    }


    public TextField getStorageTextField() {
        return storageTextField;
    }

    public TextField getPriceTextField() {
        return priceTextField;
    }

    public TextField getNameProductTextField(){
        return nameProductTextField;
    }

    public Labeled getLabelUserLogged() {
        return utilisateurLabel;
    }

    public TextField getOldShelfTxt() {
        return oldShelfTxt;
    }




}


