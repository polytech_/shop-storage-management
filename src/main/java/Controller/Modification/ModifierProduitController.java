package Controller.Modification;

import Controller.Gestion.GestionProduitController;
import Controller.Gestion.GestionProduitVendeurController;
import DAO.ProductDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

import static Alert.Alert.allertDialog;

public class ModifierProduitController {

    GestionProduitVendeurController gestionProduitVendeurController;

    public void setGestionProduitVendeurController(GestionProduitVendeurController gestionProduitVendeurController) {
        this.gestionProduitVendeurController = gestionProduitVendeurController;
    }

    GestionProduitController gestionProduitController;

    public void setGestionProduitController(GestionProduitController gestionProduitController) {
        this.gestionProduitController = gestionProduitController;
    }

    private ProductDAO productDAO = new ProductDAO();

    @FXML
    private TextField nameProductTextField;

    @FXML
    private Label utilisateurLabel;

    @FXML
    private TextField priceTextField;

    @FXML
    private TextField storageTextField;

    @FXML
    private TextField sectionTxt;

    public TextField getNameProductTextField() {
        return nameProductTextField;
    }

    public Label getUtilisateurLabel() {
        return utilisateurLabel;
    }

    public TextField getPriceTextField() {
        return priceTextField;
    }

    public TextField getStorageTextField() {
        return storageTextField;
    }

    public TextField getSectionTxt() {
        return sectionTxt;
    }


    @FXML
    void modifierProduitSalesMan(ActionEvent event) throws SQLException {

        productDAO.modifyProduct(nameProductTextField.getText(),
                            Double.parseDouble(priceTextField.getText()),
                            Long.parseLong(storageTextField.getText()));

        //Mise à jour de la table de gestion des produits
        gestionProduitVendeurController.getDonnees().clear();
        gestionProduitVendeurController.getDonnees().setAll(productDAO.listProducts());

        //On récupère la fenêtre en cours
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        allertDialog("Succès", Alert.AlertType.INFORMATION, "Produit modifié !");

    }

    @FXML
    void modifierProduitManager(ActionEvent event) throws SQLException {

        productDAO.modifyProduct(nameProductTextField.getText(),
                Double.parseDouble(priceTextField.getText()),
                Long.parseLong(storageTextField.getText()));

        //Mise à jour de la table de gestion des produits
        gestionProduitController.getDonnees().clear();
        gestionProduitController.getDonnees().setAll(productDAO.listProducts());

        //On récupère la fenêtre en cours
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        //On la ferme
        stage.close();

        allertDialog("Succès", Alert.AlertType.INFORMATION, "Produit modifié !");

    }






}
