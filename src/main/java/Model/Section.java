package Model;


public class Section {

    private String nameSection;

    public Section() {
    }

    public Section(String name) {
        this.nameSection = name;

    }

    public String getNameSection() {
        return nameSection;
    }

}
