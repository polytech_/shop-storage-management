package Model;

public class Manager extends Model.Employee {

    public Manager() {
        super();
    }

    public Manager(String name, String username, String password) {
        super(name, username, password);
    }

    @Override
    public String toString() {
        return super.toString();
    }


    @Override
    public boolean equals(Object manager) {

        if (manager == null) return false;

        if (this.getClass() != manager.getClass()) return false;

        else {
            return (name.equals(((Manager)manager).getName()) &&
                    username.equals(((Manager)manager).getUsername()) &&
                    password.equals(((Manager)manager).getPassword()));
        }

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
