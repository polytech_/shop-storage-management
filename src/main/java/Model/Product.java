package Model;

public class Product {

    private String nameProduct;

    private double price;
    private long storage;
    private String inSection;



    public Product() {
    }



    public Product(String nameProduct, double price, long storage, String inSection) {
        this.nameProduct=nameProduct;
        this.price = price;
        this.storage = storage;
        this.inSection = inSection;
    }




    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getStorage() {
        return storage;
    }

    public void setStorage(long storage) {
        this.storage = storage;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getInSection() {
        return inSection;
    }

    public void setInSection(String inSection) {
        this.inSection = inSection;
    }


    @Override
    public boolean equals(Object product) {

        if (product == null)
            return false;

        if (this.getClass() != product.getClass())
            return false;
        else {
            return (nameProduct.equals(((Product)product).getNameProduct()) &&
                    price==((Product)product).getPrice() &&
                    storage==((Product)product).getStorage() &&
                    inSection.equals(((Product)product).getInSection()));
        }
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = nameProduct.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (storage ^ (storage >>> 32));
        result = 31 * result + inSection.hashCode();
        return result;
    }



}
