package Model;

public class SalesMan extends Model.Employee {

    private String nameSection;

    public SalesMan() {
        super();
    }


    public SalesMan(String name, String username, String password, String nameSection) {
        super(name, username, password);
        this.nameSection = nameSection;
    }

    public String getNameSection() {
        return nameSection;
    }

    public void setNameSection(String nameSection) {
        this.nameSection = nameSection;
    }

    @Override
    public String toString() {

        return "Employee{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nameSection='" + nameSection + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object salesMan) {
        if (salesMan == null)
            return false;

        if (this.getClass() != salesMan.getClass())
            return false;

        else {
            return (name.equals(((SalesMan)salesMan).getName()) &&
                    username.equals(((SalesMan)salesMan).getUsername()) &&
                    password.equals(((SalesMan)salesMan).getPassword()) &&
                    nameSection.equals(((SalesMan)salesMan).getNameSection()));
        }

    }

    @Override
    public int hashCode() {
        return nameSection.hashCode();
    }
}
