package DAO;
import Model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {


    public int addProduct(Product product) throws SQLException {

        try (Connection connection = DAO.ProcessingDB.connect();
             PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO product(nameProduct, price, storage, productSection) VALUES(?,?,?,?)");
        ){

            preparedStatement.setString(1, product.getNameProduct());
            preparedStatement.setDouble(2, product.getPrice());
            preparedStatement.setLong(3, product.getStorage());
            preparedStatement.setString(4, product.getInSection());

            return preparedStatement.executeUpdate();

        }
    }



    public Product getProduct(String productName) throws SQLException {
        
        ResultSet resultSet =null;

        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product where nameProduct = ?")){

            preparedStatement.setString(1, productName);
            resultSet = preparedStatement.executeQuery();
            
            if(resultSet.next()) {
                return new Product(resultSet.getString("nameProduct"),
                        resultSet.getInt("price"),
                        resultSet.getInt("storage"),
                        resultSet.getString("productSection"));
            }

        } finally {
            if(resultSet!=null) resultSet.close();
        }

        return null;

    }




    public List<Product> listProducts() throws SQLException {

        try (Connection connection = ProcessingDB.connect();
             ResultSet resultSet = connection.createStatement().executeQuery(
                "SELECT * FROM product;")){

            ArrayList<Product> listProducts = new ArrayList<>();

            while(resultSet.next()) {
                listProducts.add(new Product(
                        resultSet.getString("nameProduct"),
                        resultSet.getDouble("price"),
                        resultSet.getInt("storage"),
                        resultSet.getString("productsection")));
            }

            return listProducts;
        }


    }




    public void deleteProduct(String nameProduct) throws SQLException {

        try (Connection connection = ProcessingDB.connect();
             PreparedStatement   preparedStatement = connection.prepareStatement("DELETE FROM product WHERE nameProduct ='"+nameProduct+"'");
        ){

            preparedStatement.executeUpdate();

        }
    }




    public void transferProduct(String productName, String newSection) throws SQLException {

        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement =
                    connection.prepareStatement(
                            "UPDATE product SET productSection = ? WHERE nameProduct = ?")) {

            // set the corresponding param
            preparedStatement.setString(1, newSection);
            preparedStatement.setString(2, productName);
            // update
            preparedStatement.executeUpdate();

        }

    }




    public int countProduct() throws SQLException {

        try(Connection connection = ProcessingDB.connect();
            Statement statement = connection.createStatement();
            ResultSet r = statement.executeQuery("SELECT COUNT(*) AS nbProducts FROM product")) {

            r.next();
            return r.getInt("nbProducts");

        }

    }



    public String getSectionOfProduct(String name) throws SQLException {
        ResultSet resultSet = null;
        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT productSection" +
                    " from product where nameProduct=?"))
        {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                return resultSet.getString("productSection");
            }


        }finally {
            if(resultSet!=null) resultSet.close();
        }

        return null;
    }



    public void modifyProduct(String name, double newPrice, long newStorage) throws SQLException {

        try(Connection connection = ProcessingDB.connect();
            PreparedStatement statement = connection.prepareStatement("UPDATE product " +
                    "SET price = ?, storage = ? " +
                    "WHERE nameProduct = ?")) {

            statement.setDouble(1, newPrice);
            statement.setLong(2, newStorage);
            statement.setString(3, name);

            statement.executeUpdate();
        }
    }



    public double selectPrice(String name) throws SQLException {

        ResultSet resultSet = null;
        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT price" +
                    " from product where nameProduct=?"))
        {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                return resultSet.getDouble("price");
            }

        }finally {
            if(resultSet!=null) resultSet.close();
        }

        return -1;
    }




    public long selectStorage(String name) throws SQLException {

        ResultSet resultSet = null;
        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT storage" +
                    " from product where nameProduct=?"))
        {
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                return resultSet.getLong("storage");
            }

        }finally {
            if(resultSet!=null) resultSet.close();
        }

        return -1;
    }

}
