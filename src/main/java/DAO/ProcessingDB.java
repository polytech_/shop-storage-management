package DAO;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class ProcessingDB {

    private static Logger logger = Logger.getLogger("myLogger");



    public static Connection connect() throws SQLException {

        //                  >>>>>>>>>>>>>>    IMPORTANT   <<<<<<<<<<<<<<<<<<

        //Merci de choisir l'url en fonction de l'action voulue (usage de l'appli ou execution des tests) :

        //Pour un usage normal de l'appl, merci d'utiliser l'url si dessous
        //String url = "jdbc:sqlite:src/main/java/Database/shopDatabase.db";

        //Pour executer les tests, merci d'utiliser l'url suivant :
        String url = "jdbc:sqlite:src/test/testDB.db";

        // création de la connexion
        return DriverManager.getConnection(url);
    }


    public static void rollbackTestDB() {


        try(FileChannel source = new FileInputStream(new File("src/main/java/Database/refDB.db")).getChannel();
            FileChannel destination = new FileOutputStream(new File("src/test/testDB.db")).getChannel()) {

            destination.transferFrom(source, 0, source.size());
        }
        catch (IOException e) {
            logger.info(e.getMessage());
        }
    }


    public static void main(String[] args)  {
        rollbackTestDB();
    }


}
