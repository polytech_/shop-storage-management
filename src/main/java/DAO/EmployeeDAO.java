package DAO;

import Model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
    
    public int addSalesMen(SalesMan salesMan) throws SQLException {

        try (Connection connection = ProcessingDB.connect();
        PreparedStatement preparedStatement =
                connection.prepareStatement(
                        "INSERT INTO salesMan(username, nameEmployee, passwd, employeeSection) VALUES(?,?,?,?)")){

            preparedStatement.setString(1, salesMan.getUsername());
            preparedStatement.setString(2, salesMan.getName());
            preparedStatement.setString(3, salesMan.getPassword());
            preparedStatement.setString(4, salesMan.getNameSection());

            return preparedStatement.executeUpdate();
        }
    }


    public boolean loginVerifSalesMan(String username, String passwd) throws SQLException {

        ResultSet resultSet = null;
        try (Connection connection = ProcessingDB.connect();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM salesMan where username=? and passwd=?")) {

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, passwd);

            resultSet = preparedStatement.executeQuery();
            return resultSet.next();

        }
        finally {
            if(resultSet!=null) resultSet.close();
        }


    }


    public boolean loginVerifManager(String username, String password) throws SQLException {

        ResultSet resultSet = null;

        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement =
                connection.prepareStatement("SELECT * FROM manager where username=? and password=?")) {

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
        finally {
            if(resultSet!=null) resultSet.close();

        }


    }


    public Manager getManager() throws SQLException {

        try(Connection connection = ProcessingDB.connect();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery( "SELECT * FROM manager")){

            if(resultSet.next()) {
                return new Manager(resultSet.getString("nameManager"),
                        resultSet.getString("username"),
                        resultSet.getString("password"));
            }

            else return null;

        }


    }


    public SalesMan getSalesMan(String username) throws SQLException {

        ResultSet resultSet = null;

        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM salesman where username = ?")){

            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                return new SalesMan(resultSet.getString("nameEmployee"),
                        resultSet.getString("username"),
                        resultSet.getString("passwd"),
                        resultSet.getString("employeeSection"));
            }

            else return null;

        } finally {
            if(resultSet!=null) resultSet.close();


        }

    }


    public String getSectionOfSalesMan(String username) throws SQLException {

        ResultSet resultSet = null;
        try(Connection connection = ProcessingDB.connect();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT employeeSection" +
                " from salesMan where username=?");
        )
        {
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                ProcessingDB.connect().close();
                return resultSet.getString("employeeSection");
            }
            else return null;

        }
        finally {
            if(resultSet!=null) resultSet.close();
        }


    }


    public String getUsernameOfSalesMan(String name) throws SQLException {

        ResultSet resultSet = null;

        try(Connection conn =  ProcessingDB.connect();
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT username from salesMan where nameEmployee = ?")){

            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                return resultSet.getString("username");
            }
            else return null;

        } finally {
            if(resultSet!=null) resultSet.close();
        }

    }


    public List<SalesMan> listSalesMen() throws SQLException{

        ArrayList<SalesMan> listSalesMen = new ArrayList<>();



        try(Connection conn = ProcessingDB.connect();
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM salesMan;")){

            while(resultSet.next()) {
                listSalesMen.add(new SalesMan(
                        resultSet.getString("nameEmployee"),
                        resultSet.getString("username"),
                        resultSet.getString( "passwd"),
                        resultSet.getString("employeeSection")));
            }

        }

        return listSalesMen;
    }


    public void deleteSalesMan(String username) throws SQLException {
        try(Connection conn = ProcessingDB.connect();
            PreparedStatement preparedStatement = conn.prepareStatement("DELETE FROM salesman WHERE username='"+username+"'")) {
            preparedStatement.executeUpdate();
        }
    }


    public int countSalesMen() throws SQLException{

        try(Connection connection = ProcessingDB.connect();
            Statement statement = connection.createStatement();
            ResultSet r = statement.executeQuery("SELECT COUNT(*) AS nbSalesMen FROM salesman")) {

                r.next();
                return r.getInt("nbSalesMen");

        }

    }


    public void transferSalesMan(String username, String newSection) throws SQLException  {

        String sql = "UPDATE salesman SET employeesection = ? "
                + "WHERE username = ?";

        try (Connection conn = ProcessingDB.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, newSection);
            pstmt.setString(2, username);

            // update
            pstmt.executeUpdate();

        }
    }

}
