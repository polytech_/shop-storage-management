package DAO;
import Model.Section;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SectionDAO {

    public SectionDAO()  {
        //Controlleur vide
    }

    public List<String> listSections() throws SQLException{


        try( ResultSet resultSet = ProcessingDB.connect()
                    .createStatement()
                .executeQuery( "SELECT * FROM Section")){

            ArrayList<String> listSection = new ArrayList<>();


            while(resultSet.next()) {
                listSection.add(new Section(resultSet.getString("sectionName")).getNameSection());
            }

            return listSection;
        }

    }


    public boolean verifySectionExistence(String section) throws SQLException {
        ResultSet resultSet = null;
        try(Connection connection = ProcessingDB.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM section WHERE sectionName = ?")) {

            preparedStatement.setString(1, section);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next();

        } finally {
            if(resultSet!=null) resultSet.close();
        }

    }
}
