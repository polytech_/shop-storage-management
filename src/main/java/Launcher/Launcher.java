package Launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Launcher extends Application {


    @Override
    public void start(Stage primaryStage) throws IOException {


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Authentification.fxml"));
        Parent root = loader.load();


        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Home");
        primaryStage.setResizable(false);
        primaryStage.show();




    }
}
