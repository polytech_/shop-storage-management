package Alert;

public class Alert {

    //Methode pour la gestion des erreurs (alertes)
    public static void allertDialog(String header, javafx.scene.control.Alert.AlertType alertType, String message) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(alertType);
        alert.setTitle("Dialog info");
        alert.setHeaderText(header);
        alert.setContentText(message);
        alert.showAndWait();
    }


}
