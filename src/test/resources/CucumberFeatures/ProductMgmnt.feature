Feature: product management

  @AddProduct
  Scenario: adding a product
    Given I sign in to the app as an admin "admin", "admin"
    When i add a product : "nameADD", 999, 999, "sectionTEST"
    Then a product is added : "nameADD"

  @RemoveProduct
  Scenario: removing a product
    Given I sign in to the app as an admin "admin", "admin"
    When i remove a product : "product5"
    Then a product is removed

  @MoveProduct
  Scenario: moving a product
    Given I sign in to the app as an admin "admin", "admin"
    Then I verify if section "sectionTEST" exists
    When i move a product : "product7" to the section "sectionTEST"
    Then a product is moved to the new section






