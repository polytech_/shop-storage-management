
Feature: salesmen management

  @AddSalesMan
  Scenario: adding a salesman
    Given I sign in to the app as an admin "admin", "admin"
    When i add a salesman : "usernameADD", "nameADD", "passwdADD", "section3"
    Then a salesman is added : "usernameADD"

  @RemoveSalesMan
  Scenario: removing a salesman
    Given I sign in to the app as an admin "admin", "admin"
    When i remove a salesman : "usernameDEL"
    Then a salesman is removed

  @MoveSalesMan
  Scenario: moving a salesman
    Given I sign in to the app as an admin "admin", "admin"
    Then I verify if section "sectionTEST" exists
    When i move a salesman : "usernameTRANSF" to the section "sectionTEST"
    Then a salesman is moved to the new section






