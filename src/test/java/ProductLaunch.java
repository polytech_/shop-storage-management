import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin ="html:src/test/report/Product",
        monochrome = true,
        features = "src/test/resources/CucumberFeatures/ProductMgmnt.feature",
        tags={"@AddProduct or @RemoveProduct or @MoveProduct"}

)
public class ProductLaunch {}
