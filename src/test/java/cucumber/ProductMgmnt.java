package cucumber;

import DAO.ProcessingDB;
import DAO.ProductDAO;
import Model.Product;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.IOException;
import java.sql.SQLException;

public class ProductMgmnt {

    ProductDAO productDA0 = new ProductDAO();

    //Scenario 1
    @When("i add a product : {string}, {int}, {int}, {string}")
    public void i_add_a_product(String name, Integer price, Integer storage, String section) throws SQLException {
        Assert.assertNotEquals(0, productDA0.addProduct(new Product(name, price, storage, section)));
    }

    @Then("a product is added : {string}")
    public void a_product_is_added(String name) throws IOException, SQLException {
        Assert.assertNotNull(productDA0.getProduct(name));
        ProcessingDB.rollbackTestDB();
    }



    //Scenario 2
    @When("i remove a product : {string}")
    public void i_remove_a_product(String name) throws SQLException {
        productDA0.deleteProduct(name);
    }

    @Then("a product is removed")
    public void a_product_is_removed() throws IOException, SQLException {
        Assert.assertNull(productDA0.getProduct("product5"));
        ProcessingDB.rollbackTestDB();
    }


    //Scenario 3
    @When("i move a product : {string} to the section {string}")
    public void i_move_a_product_to_the_section(String name, String newSection) throws SQLException {
        productDA0.transferProduct(name, newSection);
    }

    @Then("a product is moved to the new section")
    public void a_product_is_moved_to_the_new_section() throws IOException, SQLException {
        Assert.assertEquals("sectionTEST", productDA0.getSectionOfProduct("product7"));
        ProcessingDB.rollbackTestDB();
    }




}
