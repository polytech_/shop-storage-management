package cucumber;
import DAO.EmployeeDAO;
import DAO.ProcessingDB;
import DAO.SectionDAO;
import Model.SalesMan;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.IOException;
import java.sql.SQLException;


public class SalesManMgmnt {

    private EmployeeDAO employeeDAO = new EmployeeDAO();
    private SectionDAO sectionDAO = new SectionDAO();

    //Scenario 1
    @Given("I sign in to the app as an admin {string}, {string}")
    public void i_sign_in_to_the_app_as_an_admin(String username, String password) throws SQLException {
        Assert.assertTrue(employeeDAO.loginVerifManager(username, password));
    }

    @When("i add a salesman : {string}, {string}, {string}, {string}")
    public void i_add_a_salesman(String username,
                                 String name,
                                 String passwd,
                                 String section) throws SQLException {

        Assert.assertNotEquals(0, employeeDAO.addSalesMen(new SalesMan(name, username, passwd, section)));
    }

    @Then("a salesman is added : {string}")
    public void a_salesman_is_added(String username) throws IOException, SQLException {
        Assert.assertNotNull(employeeDAO.getSalesMan(username));
        ProcessingDB.rollbackTestDB();
    }



    //Scenario 2
    @When("i remove a salesman : {string}")
    public void i_remove_a_salesman(String username) throws SQLException {
        employeeDAO.deleteSalesMan(username);
    }

    @Then("a salesman is removed")
    public void a_salesman_is_removed() throws IOException, SQLException {
        Assert.assertNull(employeeDAO.getSalesMan("usernameDEL"));
        ProcessingDB.rollbackTestDB();
    }



    //Scenario 3
    @Then("I verify if section {string} exists")
    public void i_verify_if_section_exists(String section) throws SQLException {
        Assert.assertTrue(sectionDAO.verifySectionExistence(section));
    }

    @When("i move a salesman : {string} to the section {string}")
    public void i_move_a_salesman_to_the_shelf(String username, String newShelf) throws SQLException {
        employeeDAO.transferSalesMan(username, newShelf);
    }

    @Then("a salesman is moved to the new section")
    public void a_salesman_is_moved() throws IOException, SQLException {
        Assert.assertEquals("sectionTEST", employeeDAO.getSectionOfSalesMan("usernameTRANSF"));
        ProcessingDB.rollbackTestDB();
    }





}
