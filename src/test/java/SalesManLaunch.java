import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin ="html:src/test/report/SalesMan",
        monochrome = true,
        features = "src/test/resources/CucumberFeatures/SalesManMgmnt.feature",
        tags =  {"@AddSalesMan or @RemoveSalesMan or @MoveSalesMan"}
        )
public class SalesManLaunch {}
