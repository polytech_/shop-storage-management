package UnitTests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import DAO.EmployeeDAO;
import DAO.ProcessingDB;
import Model.Manager;
import Model.SalesMan;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;



@RunWith(MockitoJUnitRunner.class)
public class EmployeeDAOTest {


    private EmployeeDAO employeeDAO = new EmployeeDAO();

    @Mock
    SalesMan salesManMock;

    @Mock
    ArrayList<SalesMan> mockedSalesMen;


    @Before
    public void setUp() {

        salesManMock = new SalesMan();
        salesManMock.setUsername("testUsername");
        salesManMock.setName("testSalesMan");
        salesManMock.setPassword("testPasswd");
        salesManMock.setNameSection("testSection");


        //Préparation de la liste des employés
        when(mockedSalesMen.get(0)).thenReturn(
                new SalesMan("name1",
                        "username1",
                        "passwd1",
                        "section1"));

        when(mockedSalesMen.get(1)).thenReturn(
                new SalesMan("name2",
                        "unsername2",
                        "passwd2",
                        "section2"));

        when(mockedSalesMen.get(2)).thenReturn(
                new SalesMan("nameDEL",
                        "usernameDEL",
                        "passwdDEL",
                        "section5"));

        when(mockedSalesMen.get(3)).thenReturn(
                new SalesMan("nameTRANSF",
                        "usernameTRANSF",
                        "passwdTRANSF",
                        "section3"));

    }



    @Test
    public void test_addSalesMen() throws SQLException {
        int beforeAdd = employeeDAO.countSalesMen();
        employeeDAO.addSalesMen(salesManMock);
        Assert.assertEquals(beforeAdd+1, employeeDAO.countSalesMen());
        ProcessingDB.rollbackTestDB();
    }



//    @Test(expected = SQLException.class)
//    public void testException_addSalesMen() throws SQLException {
//        employeeDAO.addSalesMen(salesManMock);
//        ProcessingDB.rollbackTestDB();
//    }



    @Test
    public void test_loginVerifSalesMan() throws SQLException {
        Assert.assertTrue(employeeDAO.loginVerifSalesMan("username1", "passwd1"));
    }



    @Test
    public void test_loginVerifManager() throws SQLException {
        Assert.assertTrue(employeeDAO.loginVerifManager("admin", "admin"));
    }



    @Test
    public void test_getSalesMan() throws SQLException {
        SalesMan salesManToGet = new SalesMan(
                "name1",
                "username1",
                "passwd1",
                "section1");

        Assert.assertEquals(salesManToGet, employeeDAO.getSalesMan("username1"));
    }



    @Test
    public void test_getManager() throws SQLException {
        Manager managerToGet = new Manager(
                "adminName",
                "admin",
                "admin");
        Assert.assertEquals(managerToGet, employeeDAO.getManager());
    }



    @Test
    public void test_getSectionOfSalesMan() throws SQLException {

        Assert.assertEquals("section1",
                    employeeDAO.getSectionOfSalesMan("username1"));
    }


    @Test
    public void test_getUsernameOfSalesMan() throws SQLException {
        Assert.assertEquals("username1",
                employeeDAO.getUsernameOfSalesMan("name1"));
    }


    @Test
    public void test_listSalesMen() throws SQLException {
        Assert.assertTrue(
                (mockedSalesMen.get(0).equals(employeeDAO.listSalesMen().get(0))) &&
                        (mockedSalesMen.get(1).equals(employeeDAO.listSalesMen().get(1))) &&
                        (mockedSalesMen.get(2).equals(employeeDAO.listSalesMen().get(2))) &&
                        (mockedSalesMen.get(3).equals(employeeDAO.listSalesMen().get(3)))
        );
    }


    @Test
    public void test_deleteSalesMan() throws SQLException {
        int beforeDelete = employeeDAO.countSalesMen();
        employeeDAO.deleteSalesMan("usernameDEL");
        Assert.assertEquals(beforeDelete-1, employeeDAO.countSalesMen());
        ProcessingDB.rollbackTestDB();
    }


    @Test
    public void test_transferSalesMan() throws SQLException {
        employeeDAO.transferSalesMan("usernameTRANSF", "testSection");
        Assert.assertEquals("testSection", employeeDAO.getSectionOfSalesMan("usernameTRANSF"));
        ProcessingDB.rollbackTestDB();
    }


    @Test
    public void test_equals_hashcode_salesMan() {
        SalesMan SM1 = new SalesMan("SM1", "USM1", "PSM1", "SSM1");
        SalesMan SM1_test = new SalesMan("SM1", "USM1", "PSM1", "SSM1");
        Assert.assertTrue(SM1.equals(SM1_test) && SM1_test.equals(SM1));
        Assert.assertTrue(SM1.hashCode() == SM1_test.hashCode());
    }


    @Test
    public void test_equals_hashcode_manager() {
        Manager M1 = new Manager("M1", "UM1", "PM1");
        Manager M1_test = new Manager("M1", "UM1", "PM1");
        Assert.assertTrue(M1.equals(M1_test) && M1_test.equals(M1));
        Assert.assertTrue(M1.hashCode() == M1_test.hashCode());
    }





































}









