package UnitTests;

import DAO.SectionDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.sql.SQLException;
import java.util.ArrayList;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class SectionDAOTest {

    private SectionDAO sectionDAO = new SectionDAO();

    @Mock
    ArrayList<String> mockedSections;


    @Before
    public void setUp() {

        //Préparation de la liste des rayons
        when(mockedSections.get(0)).thenReturn("section1");

        when(mockedSections.get(1)).thenReturn("section2");

        when(mockedSections.get(2)).thenReturn("section3");

        when(mockedSections.get(3)).thenReturn("section4");

        when(mockedSections.get(4)).thenReturn("section5");

        when(mockedSections.get(5)).thenReturn("sectionTEST");

    }


    @Test
    public void test_listSalesMen() throws SQLException {
        Assert.assertTrue(
                (mockedSections.get(0).equals(sectionDAO.listSections().get(0))) &&
                        (mockedSections.get(1).equals(sectionDAO.listSections().get(1))) &&
                        (mockedSections.get(2).equals(sectionDAO.listSections().get(2))) &&
                        (mockedSections.get(3).equals(sectionDAO.listSections().get(3))) &&
                        (mockedSections.get(4).equals(sectionDAO.listSections().get(4)))
        );
    }

    @Test
    public void test_verifySectionExistence() throws SQLException {
        Assert.assertTrue(sectionDAO.verifySectionExistence("sectionTEST"));
    }

    




































}









