package UnitTests;

import DAO.ProductDAO;
import Model.Product;
import DAO.ProcessingDB;
import Model.SalesMan;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class ProductDAOTest {
    private ProductDAO productDAO = new ProductDAO();

    @Mock
    Product productMock;

    @Mock
    ArrayList<Product> mockedProducts;

    @Before
    public void setUp(){
        Double testPrice = 35.5;
        Long testStorage = 150L;

        productMock = new Product();
        productMock.setNameProduct("testNameProduct");
        productMock.setInSection("testInsectionProduct");
        productMock.setPrice(testPrice);
        productMock.setStorage(testStorage);

        //Liste de produits
        when(mockedProducts.get(0)).thenReturn(
                new Product("product1",12.99,200,"section2")
        );
        when(mockedProducts.get(1)).thenReturn(
                new Product("product2", 222, 50,"section2")
        );
        when(mockedProducts.get(2)).thenReturn(
                new Product("product3", 199, 65,"section3")
        );
        when(mockedProducts.get(3)).thenReturn(
                new Product("product4", 165, 120,"section5")
        );
        when(mockedProducts.get(4)).thenReturn(
                new Product("product5", 45, 310,"section4")
        );

    }


    @Test
    public void test_addProduct() throws SQLException {
        int beforeAdd = productDAO.countProduct();
        productDAO.addProduct(productMock);
        Assert.assertEquals(beforeAdd+1, productDAO.countProduct());
        ProcessingDB.rollbackTestDB();
    }

    @Test
    public void test_getProduct() throws SQLException {
        Product salesManToGet = new Product(
                "produit80",
                35,
                150,
                "section1");
        Assert.assertEquals(salesManToGet, productDAO.getProduct("produit80"));
    }


    @Test
    public void test_deleteProduct() throws SQLException {
        int beforeDelete = productDAO.countProduct();
        productDAO.deleteProduct("product3");
        Assert.assertEquals(beforeDelete-1,productDAO.countProduct());
        ProcessingDB.rollbackTestDB();
    }


    @Test
    public void test_transferProduct() throws SQLException {
        productDAO.transferProduct("product3","testSection");
        Assert.assertEquals("testSection", productDAO.getSectionOfProduct("product3"));
        ProcessingDB.rollbackTestDB();
    }



    @Test
    public void test_listProduct() throws SQLException {

        Assert.assertTrue(
                (mockedProducts.get(0).equals(productDAO.listProducts().get(0))) &&
                        (mockedProducts.get(1).equals(productDAO.listProducts().get(1))) &&
                        (mockedProducts.get(2).equals(productDAO.listProducts().get(2))) &&
                        (mockedProducts.get(3).equals(productDAO.listProducts().get(3))) &&
                        (mockedProducts.get(4).equals(productDAO.listProducts().get(4)))
        );
    }


    @Test
    public void test_modifyProduct() throws SQLException {
        productDAO.modifyProduct("product1", 999, 999);
        Assert.assertEquals(999, productDAO.selectPrice("product1"), 0);
        Assert.assertEquals(999, productDAO.selectStorage("product1"));
        ProcessingDB.rollbackTestDB();
    }


    @Test
    public void test_equals_hashcode_product() {
        Product P1 = new Product("P1", 999, 999, "SP1");
        Product P1_test = new Product("P1", 999, 999, "SP1");
        Assert.assertTrue(P1.equals(P1_test) && P1_test.equals(P1));
        Assert.assertTrue(P1.hashCode() == P1_test.hashCode());
        Assert.assertTrue(P1.equals(P1_test) && P1_test.equals(P1));
    }
}
