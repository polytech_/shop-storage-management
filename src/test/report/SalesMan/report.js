$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/CucumberFeatures/SalesManMgmnt.feature");
formatter.feature({
  "name": "salesmen management",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "adding a salesman",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddSalesMan"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i add a salesman : \"usernameADD\", \"nameADD\", \"passwdADD\", \"section3\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_add_a_salesman(java.lang.String,java.lang.String,java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a salesman is added : \"usernameADD\"",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.a_salesman_is_added(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "removing a salesman",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@RemoveSalesMan"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i remove a salesman : \"usernameDEL\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_remove_a_salesman(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a salesman is removed",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.a_salesman_is_removed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "moving a salesman",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@MoveSalesMan"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I verify if section \"sectionTEST\" exists",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_verify_if_section_exists(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i move a salesman : \"usernameTRANSF\" to the section \"sectionTEST\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_move_a_salesman_to_the_shelf(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a salesman is moved to the new section",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.a_salesman_is_moved()"
});
formatter.result({
  "status": "passed"
});
});