$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/CucumberFeatures/ProductMgmnt.feature");
formatter.feature({
  "name": "product management",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "adding a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i add a product : \"nameADD\", 999, 999, \"sectionTEST\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.i_add_a_product(java.lang.String,java.lang.Integer,java.lang.Integer,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a product is added : \"nameADD\"",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.a_product_is_added(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "removing a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@RemoveProduct"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i remove a product : \"product5\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.i_remove_a_product(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a product is removed",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.a_product_is_removed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "moving a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@MoveProduct"
    }
  ]
});
formatter.step({
  "name": "I sign in to the app as an admin \"admin\", \"admin\"",
  "keyword": "Given "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_sign_in_to_the_app_as_an_admin(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I verify if section \"sectionTEST\" exists",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.SalesManMgmnt.i_verify_if_section_exists(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i move a product : \"product7\" to the section \"sectionTEST\"",
  "keyword": "When "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.i_move_a_product_to_the_section(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a product is moved to the new section",
  "keyword": "Then "
});
formatter.match({
  "location": "cucumber.ProductMgmnt.a_product_is_moved_to_the_new_section()"
});
formatter.result({
  "status": "passed"
});
});